import { Component, Input, OnInit } from '@angular/core';
import { Poll } from '../../../../core/models/poll.model';
import { UntypedFormGroup } from '@angular/forms';

@Component({
	selector: 'app-kind-select',
	templateUrl: './kind-select.component.html',
	styleUrls: ['./kind-select.component.scss'],
})
export class KindSelectComponent implements OnInit {
	public template_questions_answers = true;
	@Input()
	public poll?: Poll;
	@Input()
	public form: UntypedFormGroup;

	constructor() {}

	ngOnInit(): void {}
}
